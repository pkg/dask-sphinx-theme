
# This file was generated by 'versioneer.py' (0.21) from
# revision-control system data, or from the parent directory name of an
# unpacked source archive. Distribution tarballs contain a pre-generated copy
# of this file.

import json

version_json = '''
{
 "date": "2022-06-16T12:13:28-0500",
 "dirty": false,
 "error": null,
 "full-revisionid": "e1e8065844c96d64c6baec9091f855dac241e947",
 "version": "3.0.3"
}
'''  # END VERSION_JSON


def get_versions():
    return json.loads(version_json)
